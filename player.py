#!/usr/bin/python3
"""
    "THE BEER-WARE LICENSE" (Revision 42):
    Miguel Nornkirn wrote this file.  As long as you retain this notice you
    can do whatever you want with this stuff. If we meet some day, and you think
    this stuff is worth it, you can buy me a beer in return.

    Just a few warnings:
    1. This code is trash, I hacked this as quickly as possible without any concern for readability.
    2. Using this requires the Inlite Barcode Reader CLI https://barcode-reader.inliteresearch.com/inlite-barcode-reader.php
    3. You can download it here https://docs.inliteresearch.com/barcode-reader-cli
    4. Using it requires adding the executable to the Windows path enviroment variable
    5. If you are on Linux (i.e. if you're based), add
        export PATH=path_to_BarcodeReaderCLI/bin/:$PATH
        to your .bashrc. Replace path_to_BarcodeReaderCLI/bin/ with your actual path.
    6. Then, just run this at the same folder where your your bad apple apple qr codes are. They must
        be in order and be named as bad_apple_x.png.
    7. Change the number of SCALE (variable below) to make so the window is at the size you desire.
"""
SCALE = 10

from midi2audio import FluidSynth
import pygame
import pygame.time
import pygame.mixer
import glob
import os
import math
import brotli

COLOR_WHITE = 1
COLOR_BLACK = 0

def to_normal_alts(alter):
    result = []
    i = 0 
    while i < len(alter):
        if alter[i] == 255:
            i += 1
            n = 0
            while alter[i] != 255:
                n += alter[i]
                i += 1
            result.append(n)
        else:
            result.append(alter[i])

        i += 1
    return result

def decode_qr_data(qr_bytes):
    original_alts = brotli.decompress(qr_bytes)
    original_alts = to_normal_alts(original_alts)
    current_color = COLOR_BLACK
    pixels = []
    for b in original_alts:
        for _ in range(b):
            pixels.append(current_color)

        if current_color == COLOR_WHITE:
            current_color = COLOR_BLACK
        else:
            current_color = COLOR_WHITE
    return pixels

print("Reading QR codes")

qr_bytes = []
for f in sorted(glob.glob("bad_apple_*.png")):
    n = f.split("_")[2].split('.')[0]
    of = "qr_output_" + n + ".bin"
    os.system("BarcodeReaderCLI " + f + " -o=" + of)
    fd = open(of, "rb")
    qr_bytes += list(fd.read())
    fd.close()
    print("Read file " + f)


print("Processing QR codes")
video_width = qr_bytes[0]
video_height = qr_bytes[1]
video_fps = qr_bytes[2]
# 4-byte integer
midi_len = ((qr_bytes[3] << 24) | (qr_bytes[4] << 16) |
                (qr_bytes[5] << 8) | qr_bytes[6])

i = 3 + 4
vid_i = midi_len + i
midi_bin = qr_bytes[i:vid_i]
vid_bin = qr_bytes[vid_i:]

decomp = brotli.decompress(bytes(midi_bin))
midi_f = open("temp.mid", "wb")
midi_f.write(decomp)
midi_f.close()

new_pixels = decode_qr_data(bytes(vid_bin))
WINDOW_X = SCALE * video_width
WINDOW_Y = SCALE * video_height

draw_color = {
    COLOR_WHITE: (255, 255, 255),
    COLOR_BLACK: (0, 0, 0)
}

pygame.init()
pygame.mixer.init()

FluidSynth().midi_to_audio("temp.mid", "temp.wav")

pygame.display.set_caption("Event Tester")
screen = pygame.display.set_mode((WINDOW_X, WINDOW_Y))
clock = pygame.time.Clock()
running = True
frame_c = 0
font = pygame.font.SysFont('Comic Sans MS', 12)

pygame.mixer.music.load("temp.wav")
timer = 0
while running:
    if timer != -1:
        if timer >= 15 * 1000:
            pygame.mixer.music.play()
            timer = -1
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    screen.fill("purple")

    px = 0
    py = 0
    surfaces = []
    for i in range(video_width * video_height):
        if frame_c >= len(new_pixels): # Video end
            running = False
            break
        pixel = new_pixels[frame_c]
        pygame.draw.rect(screen, draw_color[pixel], (px, py, SCALE, SCALE))
        surfaces.append(
            {"surface": font.render(str(frame_c), False, (255, 0, 0)),
             "posx": px,
             "posy": py
            }
        )

        frame_c += 1

        px += SCALE

        if px >= WINDOW_X:
            px = 0
            py += SCALE

    # Draw things
    pygame.display.flip()
    dt = clock.tick(video_fps)
    if timer != -1:
        timer += dt

pygame.mixer.quit()
pygame.quit()
