Bad apple on 8 QR codes.

# Usage

Python 3 is required, bash is optional but recommended.

Download the MIDI (for audio) from [this site](https://onlinesequencer.net/1844940#). Make sure to
save the file in the same folder as compress.py with the name "Bad Apple.mid".

Download the bad apple clip from the a video site. It's a 3:39 minutes long Bad Apple clip, like
[this one](https://www.youtube.com/watch?v=FtutLA63Cp8&pp=ygUJYmFkIGFwcGxl). The video needs to be
saved as "orig.webm". If you don't find a webm, you can use an online converter or ffmpeg to change
the format.

I'm not including these files directly on the repo for copyright reasons.

Libraries required for the Python scripts:
* cv2 (encoder-only);
* segno;
* brotli;
* pygame (player-only);
* midi2audio (player-only).

The following additional software is required for this to work:
* [Inlite Barcode Reader CLI](https://docs.inliteresearch.com/barcode-reader-cli) (you have to add
this to your PATH variable);
* ffmpeg.

If you want to encode the video yourself, run encode.py.

If you want to play the video yourself, you should read the header of player.py.

This should theoretically also work for any black-and-white videos, though they weren't tested.

# Credits

Credits to [NECROMANCER](https://onlinesequencer.net/1844940#) for making the MIDI. All credits due
to them. I wouldn't have the skill to create a cover by myself (I'm not affiliated or endorsed by
them).

Special thanks to Team Shanghai Alice for making the Touhou series.

And to everyone involved in creating the Bad Apple clip and cover.