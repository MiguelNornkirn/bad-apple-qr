#!/usr/bin/python3
"""
    "THE BEER-WARE LICENSE" (Revision 42):
    Miguel Nornkirn wrote this file.  As long as you retain this notice you
    can do whatever you want with this stuff. If we meet some day, and you think
    this stuff is worth it, you can buy me a beer in return.

    This is a hacky mess, trash code ahead.

    Your original file must be name orig.webm and the midi file should be
    named Bad Apple.mid.

    Go to the "CUSTOMIZE THINGS HERE" section if you want to customize stuff
"""
import cv2
import segno
import os
import math
import brotli

# CUSTOMIZE THINGS HERE
ORIG_W = 20
ORIG_H = 14
FPS = 10

COLOR_WHITE = 1
COLOR_BLACK = 0
QR_CODE_TYPE_40_LEN_BYTES = 2953
QR_CODE_STORAGE_BYTES = QR_CODE_TYPE_40_LEN_BYTES * 8

def abs(x):
    if x < 0:
        x *= -1
    return x

def diff(x, y):
    return abs(x - y)

def get_dist_from(r0, g0, b0, r1, g1, b1):
    x = diff(r0, r1)
    y = diff(g0, g1)
    z = diff(b0, b1)
    return math.sqrt(x * x + y * y + z * z)

def get_closest_color(r, g, b):
    b = get_dist_from(r, g, b, 0, 0, 0)
    w = get_dist_from(r, g, b, 0xff, 0xff, 0xff)

    if b < w:
        return COLOR_BLACK
    else:
        return COLOR_WHITE

def get_n(f):
    return int(f.split('_')[1].split('.')[0])

def sort_files(file_list):
    changed_any = True
    while changed_any:
        changed_any = False
        for i in range(len(file_list)):
            if i + 1 >= len(file_list):
                break

            current = file_list[i]
            next = file_list[i + 1]
            nc = get_n(current)
            nn = get_n(next)
            if nc > nn:
                file_list[i] = next
                file_list[i + 1] = current
                changed_any = True

def compress(values):
    return brotli.compress(bytes(values))

def cmp_l(l0, l1):
    print(l0)
    print(l1)
    if len(l0) != len(l1):
        print("bruh")

    for i in range(len(l0)):
        if l0[i] != l1[i]:
            return False
    return True

def alternations_to_bytes(alt):
    r = []
    for n in alt:
        if n >= 255:
            r.append(255)

            v = n
            while v >= 255:
                v -= 254
                r.append(254)

            if v > 0:
                r.append(v)

            r.append(255)
        else:
            r.append(n)
    return r

def list_str(l):
    result = "["
    k = 0
    for i in l:
        if k > 20:
            k = 0
            result += "\n"

        result += str(i) + ", "
        k += 1
    result += "]\n"
    return result

def split_to_elems(l, elen):
    result = []
    c = []
    i = 0
    for v in l:
        i += 1
        if i > elen:
            result.append(c)
            c = []
            i = 1
        c.append(v)

    result.append(c)
    return result

def to_normal_alts(alter):
    result = []
    i = 0 
    while i < len(alter):
        if alter[i] == 255:
            i += 1
            n = 0
            while alter[i] != 255:
                n += alter[i]
                i += 1
            result.append(n)
        else:
            result.append(alter[i])

        i += 1
    return result

def decode_qr_data(qr_bytes):
    original_alts = brotli.decompress(qr_bytes)
    original_alts = to_normal_alts(original_alts)
    current_color = COLOR_BLACK
    pixels = []
    for b in original_alts:
        for _ in range(b):
            pixels.append(current_color)

        if current_color == COLOR_WHITE:
            current_color = COLOR_BLACK
        else:
            current_color = COLOR_WHITE
    return pixels

# This was a shell script but I moved it here
os.system("rm -rf frames")
os.system("mkdir frames")
os.system("ffmpeg -i orig.webm -vf \"scale=%s:%s,fps=%s\" frames/c01_%%04d.jpeg" %
    (ORIG_W, ORIG_H, FPS))
print("Finished converting clip to frames")


pixel_list = []
alternations = []
last = COLOR_BLACK
counter = 0

files = os.listdir("frames/")
sort_files(files)

print("Reading frames")
for file in files:
    img = cv2.imread("frames/" + file)
    rows, cols, _ = img.shape
    for i in range(rows):
        for j in range(cols):
            r, g, b = img[i, j]
            color = get_closest_color(r, g, b)
            pixel_list.append(color)

            if color == last:
                counter += 1
            else:
                last = color
                alternations.append(counter)
                counter = 1


raw_pixel_list = pixel_list.copy()

vid_bytes = compress(alternations_to_bytes(alternations))

print("Reading MIDI")
hope = open("Bad Apple.mid", "rb")
midi_raw_bin = hope.read()
midi_file_bytes = brotli.compress(bytes(midi_raw_bin))
hope.close()

# Bytes:
# width
# height
# FPS
# [4 bytes] number of bytes of the midi
header = []
header.append(ORIG_W)
header.append(ORIG_H)
header.append(FPS)
hfc = len(midi_file_bytes)
print("Length of the MIDI: %s" % (hfc))
header.append((hfc >> 8 * 3) & 0xff) # Most significant on the left/first
header.append((hfc >> 8 * 2) & 0xff)
header.append((hfc >> 8 * 1) & 0xff)
header.append((hfc >> 8 * 0) & 0xff)
header = bytes(header)

fc = len(header) + len(vid_bytes) + len(midi_file_bytes)
fdata = header + midi_file_bytes + vid_bytes

qr_codes = split_to_elems(fdata, 2953)


print("Number of QR codes: " + str(len(qr_codes)))
bruh = 1
for l in qr_codes:
    qr_file = "bad_apple_" + str(bruh) + ".png"
    qr = segno.make(bytes(l), encoding=None, mode="byte")
    qr.save(qr_file)
    bruh += 1

print("Length of QR: " + str(len(qr_codes)))
if fc <= QR_CODE_STORAGE_BYTES:
    print("Sucess")
    print("Leftover bytes: " + str(QR_CODE_STORAGE_BYTES - fc))
else:
    print("Failure")
    print("Remaining bytes to save: " + str(fc - QR_CODE_STORAGE_BYTES))
